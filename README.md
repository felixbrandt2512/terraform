# infrastructure for aws cognito

keys can be optained from  
https://console.aws.amazon.com/iam/home#/security_credential

This script will create the cognito infrastructure with following variables:



- userpool-name
- userpool-client-name
- aws_user_id
- aws_user_secret 


callback urls are defined in **callback_urls.tfvars**

```
terraform plan -var userpool-name=<YOUR_POOL_NAME>  
               -var userpool-client-name=<YOUR_CLIENT_NAME> \
               -var aws_user_id=<YOUR_ID> \
               -var aws_user_secret=<YOUR_SECRET> \
               -var-file=callback_urls.tfvars
               -out buildplan
terraform apply "buildplan"
```

## client config
please make sure **to not use localhost callback-url** in production environments! For this client I choose
**code** und **implicit** flow and an **openid** scope.