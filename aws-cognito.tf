
variable "userpool-name" {}
variable "userpool-client-name" {}
variable "aws_user_id" {}
variable "aws_user_secret" {}
variable "callback_url" {type = "list"}

provider "aws" {
  access_key = "${var.aws_user_id}"
  secret_key = "${var.aws_user_secret}"
  region = "us-east-1"
}

resource "aws_cognito_user_pool" "pool" {
  name          = "${var.userpool-name}"  
}
resource "aws_cognito_user_pool_client" "client" {
  name = "${var.userpool-client-name}"
  user_pool_id = "${aws_cognito_user_pool.pool.id}"
  depends_on = ["aws_cognito_user_pool.pool"]
  allowed_oauth_flows = ["code","implicit"]
  allowed_oauth_scopes = ["openid"]
  callback_urls = ["${var.callback_url}"]
}
output "name" {
  value = "${aws_cognito_user_pool.pool.name}"
}

